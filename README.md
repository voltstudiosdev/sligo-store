# README #

### What is this repository for? ###

**This is the application for an online responsive e-commerce for a Canadian golf retail company, Sligo. 

The application is responsive and ready for mobile, tablet, desktop (all device sizes). 

This application utilizes Spree and allows for easy management of inventory from the administrative side.** 
**



### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact