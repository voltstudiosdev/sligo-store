# This migration comes from spree_gift_card (originally 20150720210332)
class RenameCreditsToSpreeCredits < ActiveRecord::Migration
  def change
    rename_table :credits, :spree_credits
  end
end
