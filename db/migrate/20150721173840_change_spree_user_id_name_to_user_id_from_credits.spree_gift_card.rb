# This migration comes from spree_gift_card (originally 20150716203457)
class ChangeSpreeUserIdNameToUserIdFromCredits < ActiveRecord::Migration
  def change
    rename_column :credits, :spree_user_id, :user_id
  end
end
