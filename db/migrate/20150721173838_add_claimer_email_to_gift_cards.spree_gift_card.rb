# This migration comes from spree_gift_card (originally 20150709205831)
class AddClaimerEmailToGiftCards < ActiveRecord::Migration
  def change
    add_column :gift_cards, :claimer_email, :string
  end
end
