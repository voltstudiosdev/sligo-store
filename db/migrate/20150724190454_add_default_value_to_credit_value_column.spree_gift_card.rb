# This migration comes from spree_gift_card (originally 20150724190104)
class AddDefaultValueToCreditValueColumn < ActiveRecord::Migration
  def change
    def up
      change_column :spree_credits, :amount, :decimal, :default => 0
    end
    
    def down
      change_column :spree_credits, :amount, :amount, :default => 0
    end
  end
end
