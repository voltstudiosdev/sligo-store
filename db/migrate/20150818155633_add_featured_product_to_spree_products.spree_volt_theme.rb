# This migration comes from spree_volt_theme (originally 20150817171713)
class AddFeaturedProductToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :featured_products, :boolean, default: false
  end
end
