# This migration comes from spree_gift_card (originally 20150716210344)
class AddStoreCreditAmmountToSpreeOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :store_credit_ammount, :decimal
  end
end
